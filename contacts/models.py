from django.db import models
from listings.models import Listing

# Create your models here.
class Contact(models.Model):
    name = models.CharField(max_length=200)
    listing = models.ForeignKey(Listing,on_delete=models.CASCADE)
    email = models.EmailField()
    phone = models.CharField(max_length=100)
    message = models.TextField()
    contact_date = models.DateTimeField(auto_now_add=True)
    user_id = models.IntegerField(blank=True)

    def __str__(self):
        return self.name


