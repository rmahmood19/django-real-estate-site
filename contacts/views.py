from django.contrib import messages
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from .models import Contact
from listings.models import Listing

# Create your views here.
def contact(request):
    if request.method == 'POST':
        name = request.POST['name']
        listing_id = request.POST['listing_id']
        listing = Listing.objects.get(id=listing_id)
        email = request.POST['email']
        phone = request.POST['phone']
        message = request.POST['message']
        user_id = request.POST['user_id']

        contact = Contact(name=name,listing=listing,
        email=email,phone=phone,message=message,user_id=user_id)
        contact.save()
        send_mail(
                'Contact requested',
                ('A conatct has been made by ' + name + ' on ' + listing.title),
                'info@ak.com',
                [listing.realtor.email]
                )
        messages.success(request,"your request have been submitted.")
        return redirect('/listings/'+str(listing.id))
        