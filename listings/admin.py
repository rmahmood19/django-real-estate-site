from django.contrib import admin
from .models import Listing


class ListingAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'title', 
                    'realtor', 
                    'price', 
                    'list_date',
                    'is_published')
    list_display_links = (
        'id', 
        'title'
    )
    list_filter = ('realtor', )
    list_editable = ('is_published', )
    list_per_page = 30


admin.site.register(Listing, ListingAdmin)

# Register your models here.
