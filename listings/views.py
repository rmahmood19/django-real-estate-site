from django.shortcuts import get_object_or_404, render
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from .models import Listing
from listings.choices import bedroom_choices, price_choices, state_choices


# Create your views here.
def index(request):
    listings = Listing.objects.order_by('-list_date').filter(is_published=True)
    paginator = Paginator(listings,3)
    page = request.GET.get('page')

    paged_listings = paginator.get_page(page)
  
    context = {
        'listings' : paged_listings
    }
    return render(request,'listings/listings.html', context)

def listing(request,listing_id):
    listing = get_object_or_404(Listing,pk=listing_id)
    context = {
        'listing' : listing
    }
    return render(request,'listings/listing.html',context)

def search(request):
    queryset_current = Listing.objects.order_by('-list_date')

    if 'keywords' in request.GET:
        keyword = request.GET['keywords']
        if keyword:
            queryset_current = Listing.objects.order_by('-list_date'
            ).filter(description__icontains=keyword)
    if 'city' in request.GET:
        city = request.GET['city']
        if city:
            queryset_current = Listing.objects.order_by('-list_date'
            ).filter(city__iexact=city)
    if 'state' in request.GET:
        state = request.GET['state']
        if state:
            queryset_current = Listing.objects.order_by('-list_date'
            ).filter(state__iexact=state)
    if 'bedrooms' in request.GET:
        bedrooms = request.GET['bedrooms']
        if bedrooms:
            queryset_current = Listing.objects.order_by('-list_date'
            ).filter(bedrooms__lte=bedrooms)
    if 'price' in request.GET:
        price = request.GET['price']
        if price:
            queryset_current = Listing.objects.order_by('-list_date'
            ).filter(price__lte=price)


    context = {
        'bedroom_choices' : bedroom_choices,
        'price_choices' : price_choices, 
        'state_choices' : state_choices,
        'listings' : queryset_current, 
        'values' : request.GET
        }
    return render(request,'listings/search.html',context)