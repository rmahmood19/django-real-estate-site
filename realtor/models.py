from django.db import models
from datetime import datetime

class Realtor(models.Model):
    name = models.CharField(max_length=200)
    photo = models.ImageField(upload_to='photos/realtor/')
    description = models.TextField(blank=True)
    phone = models.CharField(max_length=20)
    email = models.EmailField()
    is_mvp = models.BooleanField()
    hire_date = models.DateTimeField(default=datetime.now,blank=True)

    def __str__(self):
        return self.name
    def listing_count(self):
        return self.listing_set.count()

