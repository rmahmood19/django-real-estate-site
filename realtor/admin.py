from django.contrib import admin
from .models import Realtor

class RealtorAdmin(admin.ModelAdmin):
    list_display = ('name', 
                    'email', 
                    'hire_date', 
                    'listing_count')
    search_fields = ('name', 'email')
admin.site.register(Realtor,RealtorAdmin)

# Register your models here.
