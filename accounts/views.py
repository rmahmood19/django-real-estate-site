from django.shortcuts import render, redirect
from django.contrib import auth, messages
from django.contrib.auth.models import User
from contacts.models import Contact

# Create your views here.
def register(request):
    if request.method == 'POST':
        first_name = request.POST['first_name']
        last_name = request.POST['last_name']
        username = request.POST['username']
        email = request.POST['email']
        password = request.POST['password']
        password2 = request.POST['password2']

        #password check
        if password == password2:
            if User.objects.filter(username=username).exists():
                messages.error(request,'This username is already taken')
                return redirect('accounts:register')
            else:
                if User.objects.filter(email=email).exists():
                    messages.error(request,'This email is already registered')
                    return redirect('accounts:register')
                else:
                    user = User.objects.create_user(
                                first_name=first_name,
                                last_name=last_name,
                                username=username,
                                email=email,
                                password=password
                                )
                    user.save()
                    return redirect('accounts:login')
            
        else:
            messages.error(request,'Passwords do not match')
            return redirect('accounts:register')
        
        return redirect('accounts:register')
    else:
        return render(request,'accounts/register.html')
def login(request):
    if request.method == 'POST':
        username=request.POST['username']
        password=request.POST['password']
        user = auth.authenticate(request,
                                username=username,
                                password=password)
        if user is not None:
            auth.login(request,user)
            return redirect('accounts:dashboard')
        else:
            messages.error(request,'Username/password is not valid')
            return redirect('accounts:login')
    else:
        return render(request,'accounts/login.html')



def logout(request):
    if request.method=='POST':
        auth.logout(request)
        return redirect('index')

def dashboard(request):
    user_contacts = Contact.objects.order_by(
                            '-contact_date').filter(
                            user_id=request.user.id)
    
    context={
        'user':request.user,
        'user_contacts': user_contacts
    }
    return render(request,'accounts/dashboard.html',context)

